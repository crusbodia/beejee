-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Сен 14 2016 г., 02:37
-- Версия сервера: 5.5.49-0ubuntu0.14.04.1
-- Версия PHP: 5.5.9-1ubuntu4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `beejee`
--

-- --------------------------------------------------------

--
-- Структура таблицы `attachment`
--

CREATE TABLE IF NOT EXISTS `attachment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `review_id` int(10) unsigned DEFAULT NULL,
  `attachment_name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `reviews`
--

CREATE TABLE IF NOT EXISTS `reviews` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET latin1 NOT NULL,
  `email` varchar(50) CHARACTER SET latin1 NOT NULL,
  `text` text CHARACTER SET latin1 NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` tinyint(1) NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Дамп данных таблицы `reviews`
--

INSERT INTO `reviews` (`id`, `name`, `email`, `text`, `time`, `modified`, `status`) VALUES
(1, 'bohdan', 'crusbodia@gmail.com', 'sdlfkbjdsf', '2016-09-13 20:55:05', 0, 0),
(2, 'Daniel', 'dan@iel.dil', 'wefb werg ervujnwe;r .n ewrl;gvjn ewgvb\r\newgtb wetgb wegtrb\r\n wergv wergv wetberbnew gvber bewrg ewrg wbwerg webe rtbg ewrwer \r\nwer werg wetb ', '2016-09-13 20:58:59', 0, 0),
(3, 'Ignat', 'igmnat@elenovi.ch', 'wef gfdd pdv ldskfnl kerjnl erngbjkln ewr;jklgn w;elv', '2016-09-13 20:59:41', 1, 0),
(4, 'zero', 'zero@fi.ll', 'fimla;pd pef ikvjwd jnvk skdfj svmcx,bny tjdb ', '2016-09-13 21:00:04', 0, 0),
(5, 'hero', 'super@hero.in', 'sdf b ERGT srgb dsgb ewgtr ergtbsdF DFbvcsfadv sdgb SRgf dsgb EWRf sdfb esRGf wargb erHnbdsrg dsfbk jdfnvl ewrjgnl dsfkvn as.fmbn et5jln dsfm,c', '2016-09-13 22:10:05', 0, 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
