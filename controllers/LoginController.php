<?php

class LoginController
{
    public function actionIndex()
    {
        $login = new Login();
        $user = $login->getUserInfo();
        $user_auth = $login->checkAuth();

        require_once('../views/auth/index.php');
    }

    public function actionAuth()
    {
        $login = new Login();
        $user = $login->getUserInfo();
        $user_auth = $login->checkAuth();

        if (isset($_POST) && ! empty($_POST)) {
            if (
                isset($_POST['login']) &&
                $_POST['login'] == $user['login'] &&
                isset($_POST['password']) &&
                $user['pass'] == md5($_POST['password']) )
            {
                $_SESSION['login'] = $user['login'];

                $login = new Login();
                $user = $login->getUserInfo();
                $user_auth = $login->checkAuth();

                require_once('../views/reviews/index.php');

                return;
            }
        }

        require_once('../views/auth/index.php');
    }

    public function actionLogout()
    {
        unset($_SESSION['login']);

        require_once('../views/reviews/index.php');
    }
}