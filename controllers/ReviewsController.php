<?php

class ReviewsController
{

    public function actionIndex()
    {
        $login = new Login();
        $user_auth = $login->checkAuth();

        $review = new Reviews();
        $reviews = $review->getReviews();

        $data['title'] = "Reviews list";
        require_once('../views/reviews/index.php');

        return true;
    }

    public function actionAdd()
    {
        $login = new Login();
        $user_auth = $login->checkAuth();

        $review = new Reviews();

        if (isset($_POST) && ! empty($_POST)) {
            $response = $review->addReview($_POST);
            if ($response === true) {
                $data['info'] = "Review added. You can see it after admin confirmation";
            }

            $data['review']  = $response;
        }

        $reviews = $review->getReviews();

        $data['title']  = "Reviews list";

        require_once('../views/reviews/index.php');

        return true;
    }
}