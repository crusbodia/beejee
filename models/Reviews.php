<?php

class Reviews
{
    public $name;
    public $email;
    public $text;
    public $errors = array();

    public $db;

    public function __construct()
    {
        $this->db = Db::getInstance();
    }

    public function getReviews()
    {
        $reviews = array();

        $stmt = $this->db->pdo->query(
            "SELECT id, name, email, text, time, modified 
            FROM reviews 
            WHERE status='0'  
            ORDER BY time ASC"
        );

        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $reviews[] = $row;
        }

        return $reviews;
    }

    public function addReview($args) {
        if ($this->validate($args)) {

            if ($args['review-update-id'] != 0) {
                $stmt = $this->db->pdo->prepare(
                    "UPDATE reviews SET
                        name = :name, 
                        email  = :email, 
                        text = :text, 
                        modified = 1
                    WHERE id=:id");
                $stmt->bindParam(':id', $args['review-update-id']);
            } else {
                $stmt = $this->db->pdo->prepare(
                    "INSERT INTO reviews (name, email, text, modified, status)
                    VALUES (:name, :email, :text, 0, 0)");
            }

            $stmt->bindParam(':name', $args['name']);
            $stmt->bindParam(':email', $args['email']);
            $stmt->bindParam(':text', $args['message']);

            $stmt->execute();

            return true;
        }

        return $this;
    }

    public function updateReview($id)
    {

    }

    public function validate($args) {
        $errors = array();

        if (isset($args['name']) && isset($args['email']) && isset($args['message']) ) {
            if (empty($args['name'])) {
                $errors['name'][] = 'Name is empty';
            }

            if (empty($args['email'])) {
                $errors['email'][]= 'Email is empty';
            }
            if (! filter_var($args['email'], FILTER_VALIDATE_EMAIL)) {
                $errors['email'][]= 'Email is incorrect';
            }

            if (empty($args['message'])) {
                $errors['text'][]= 'Message is empty';
            }


            if (empty($errors['name'])) {
                $this->name = Helper::cleanStr($args['name']);
            }
            if (empty($errors['email'])) {
                $this->email = Helper::cleanStr($args['email']);
            }
            if (empty($errors['text'])) {
                $this->text = Helper::cleanStr($args['message']);
            }

        } else {
            $errors['base'][] = 'Try again';
        }

        if (count($errors) > 0) {
            $this->errors = $errors;

            return false;
        }

        return true;
    }
}