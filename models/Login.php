<?php

class Login
{
    private $userInfo;

    public function __construct()
    {
        $this->setUserInfo();
    }

    public function setUserInfo()
    {
        $this->userInfo = array(
            'id' => 1,
            'login' => 'admin',
            'pass' => md5('123')
        );
    }

    public function getUserInfo()
    {
        return $this->userInfo;
    }

    public function checkAuth()
    {
        $user = $this->getUserInfo();
        if (! empty($_SESSION['login']) && $user != false && $_SESSION['login'] == $user['login']) {
            return true;
        }

        return false;
    }
}