$(document).ready(function() {
    $(".reviews-list-table").tablesorter({
        headers: {
            4: {
                sorter: false
            }
        },
        theme : "ice",
        widgets: ['filter'],
        headerTemplate : '{content} {icon}'
    });

    $('.preview-show-btn').on('click', function () {
        var id = $(this).data('reviewItemId');
        var $reviewItem =  $('.review-item-' + id);
        var previewName = $reviewItem.find('.review-item-name').html();
        var previewEmail = $reviewItem.find('.review-item-email').html();
        var previewText = $reviewItem.find('.review-item-text').html();

        setUpPopupData(previewName, previewEmail, previewText);
    });

    $('.preview-new-review').on('click', function () {
        var name = $('#name').val();
        var email = $('#email').val();
        var text = $('#message').val();

        setUpPopupData(name, email, text);
    });

    function setUpPopupData(name, email, text) {
        $('.name-preview').html(name);
        $('.email-preview').html(email);
        $('.review-preview').html(text);
    }

    $('.edit-review-form').on('click', function () {
        var id = $(this).data('reviewItemId');
        var $reviewItem =  $('.review-item-' + id);
        var name = $reviewItem.find('.review-item-name').html();
        var email = $reviewItem.find('.review-item-email').html();
        var text = $reviewItem.find('.review-item-text').html();

        $('#name').val(name);
        $('#email').val(email);
        $('#message').val(text);

        $('#review-update-id').val(id);
    });
});