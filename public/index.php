<?php

ini_set('display_errors', 1);
error_reporting(E_ALL);

define('BATH_PATH', realpath(__DIR__ . '/..'));

// It's better to create autoload file for autoload classes
require_once('../app/Helper.php');
require_once('../app/Router.php');
require_once('../app/Db.php');

require_once('../controllers/ReviewsController.php');
require_once('../controllers/LoginController.php');
require_once('../models/Login.php');
require_once('../models/Reviews.php');
session_start();
$router = new Router();
$router->run();
