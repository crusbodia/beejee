<?php

class Router
{

    private $routes;

    public function __construct()
    {
        $routesPath = '../config/routes.php';
        $this->routes = include($routesPath);
    }


    private function getURI()
    {
        if (!empty($_SERVER['REQUEST_URI'])) {
            return trim($_SERVER['REQUEST_URI'], '/');
        }

        return false;
    }

    public function run()
    {
        $uri = $this->getURI();

        if ($uri == '') {
            $controllerName = 'ReviewsController';
            $actionName = 'actionIndex';
            $segments = array();
        } else {
            $segments = explode('/', $uri);
            $controllerName = array_shift($segments) . 'Controller';
            $controllerName = ucfirst($controllerName);
            $action = array_shift($segments);
            if (! empty($action)) {
                $actionName = 'action'.ucfirst($action);
            } else {
                $actionName = 'actionIndex';
            }
        }

        $controllerObject = new $controllerName;

        $result = call_user_func_array(array($controllerObject, $actionName), $segments);
    }
}