<?php

class Db {

//    private $connection;

    private static $instance;

    public $pdo;

    private $host;
    private $username;
    private $password;
    private $database;

    /*
    Get an instance of the Database
    @return Instance
    */
    public static function getInstance() {
        if(!self::$instance) { // If no instance then make one
            self::$instance = new self();
        }
        return self::$instance;
    }

    private function __construct() {

        if (! $this->setupDbConfig()) {
            echo "Error DB connection";

            return false;
        }
        $dsn = "mysql:host={$this->host};dbname={$this->database}";
        $db = new PDO($dsn, $this->username, $this->password);

        $this->pdo = $db;
        return $db;
    }

    private function setupDbConfig()
    {
        $paramsPath = '../config/db_config.php';
        $params = include($paramsPath);

        if (is_array($params)) {
            $this->host = $params['host'];
            $this->database = $params['dbname'];
            $this->username = $params['user'];
            $this->password = $params['password'];

            return true;
        }

        return false;
    }


    // Magic method clone is empty to prevent duplication of connection
    private function __clone() { }
    // Get mysqli connection

//    public function getConnection() {
//        return $this->instance;
//    }
}