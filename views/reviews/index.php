<?php

require_once BATH_PATH . '/views/inc/header.php'; ?>

    <div class="modal fade show-preview" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
        <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
                <div class="show-preview-wrap">
                    <h1 class="preview-title">Preview</h1>
                    <table>
                        <tr>
                            <td>Name: </td>
                            <td><div class="name-preview"></div></td>
                        </tr>
                        <tr>
                            <td>Email: </td>
                            <td><div class="email-preview"></div></td>
                        </tr>
                    </table>
                    <div class="review-preview"></div>
                </div>
            </div>
        </div>
    </div>

<?php if (isset($reviews) && is_array($reviews) && count($reviews) > 0) { ?>
    <div class="table-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <table class="reviews-list-table table table-striped table-hover tablesorter">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Review</th>
                                <th>Time</th>
                                <th>Controls</th>
                            </tr>
                        </thead>
                        <?php foreach ($reviews as $item) : ?>
                            <tr class="<?php echo ($item['modified'] == 1) ? 'success' : ''; ?> review-item review-item-<?php echo $item['id']; ?>" data-review-id="<?php echo $item['id']; ?>">
                                <td><div class="review-item-name"><?php echo $item['name']; ?></div></td>
                                <td><div class="review-item-email"><?php echo $item['email']; ?></div></td>
                                <td><div class="review-item-text"><?php echo $item['text']; ?></div></td>
                                <td><?php echo $item['time']; ?></td>
                                <td>
                                    <div class="preview preview-show-btn" data-toggle="modal" data-target=".show-preview" data-review-item-id="<?php echo $item['id']; ?>">
                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                    </div>
                                    <div class="send">
                                        <i class="fa fa-envelope-o" aria-hidden="true"></i>
                                    </div>
                                    <?php if ($user_auth) { ?>
                                        <a href="#review-form" class="edit-review-form" data-review-item-id="<?php echo $item['id']; ?>"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                    <?php } ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </table>

                    <div class="info-table">
                        <div class="green-square"></div>
                        <span> - Modified by author</span>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php } ?>

<?php if (! empty($data['info'])) { ?>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-info" role="alert">
                    <?php echo $data['info']; ?>
                </div>
            </div>
        </div>
    </div>
<?php } ?>


<?php if (isset($data['review']->errors) && count($data['review']->errors) > 0) { ?>
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="alert alert-danger" role="alert">
                    <h2>There are some errors.</h2>
                    <ul class="errors-list">
                        <?php foreach ($data['review']->errors as $error_type) {
                            foreach ($error_type as $error) {
                                echo "<li>$error</li>";
                            }
                        } ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
<?php } ?>
    <form id="review-form" action="/reviews/add" method="post">
        <input type="hidden" name="review-update-id" id="review-update-id" value="0">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group <?php echo (isset($data['review']->errors['name']) && count($data['review']->errors['name'])) ? 'has-warning' : ''; ?>">
                        <label for="name">Name</label>
                        <input type="text" class="form-control" name="name" id="name" placeholder="Name" value="<?php echo (isset($data['review']->name)) ? $data['review']->name : ''; ?>">
                    </div>
                    <div class="form-group <?php echo (isset($data['review']->errors['email']) && count($data['review']->errors['email'])) ? 'has-warning' : ''; ?>">
                        <label for="email">Email</label>
                        <input type="email" class="form-control" name="email" id="email" placeholder="Email" value="<?php echo (isset($data['review']->email)) ? $data['review']->email : ''; ?>">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group <?php echo (isset($data['review']->errors['text']) && count($data['review']->errors['text'])) ? 'has-warning' : ''; ?>">
                        <label for="message">Message</label>
                        <textarea class="form-control" rows="5" name="message" id="message"><?php echo (isset($data['review']->text)) ? $data['review']->text : ''; ?></textarea>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <button class="btn-block btn btn-info preview-new-review" type="button" data-toggle="modal" data-target=".show-preview">Preview</button>
                </div>
                <div class="col-md-6">
                    <button class="btn-block btn btn-success" type="submit">Submit</button>
                </div>
            </div>
        </div>
    </form>


<?php require_once BATH_PATH . '/views/inc/footer.php';