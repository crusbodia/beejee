<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title><?php echo (isset($data['title'])) ? $data['title'] : 'BeeJee Test'; ?></title>
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700&subset=cyrillic-ext" rel="stylesheet">
    <link rel="stylesheet" href="/assets/libs/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/assets/libs/bootstrap/css/bootstrap-theme.css">
    <link rel="stylesheet" href="/assets/libs/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="/assets/libs/tablesorter/dist/css/theme.ice.min.css">
    <link rel="stylesheet" href="/assets/css/style.css">

    <script type="text/javascript" src="/assets/libs/jquery.min.js"></script>
    <script type="text/javascript" src="/assets/libs/tablesorter/dist/js/jquery.tablesorter.js"></script>
    <script type="text/javascript" src="/assets/libs/tablesorter/dist/js/jquery.tablesorter.widgets.js"></script>
    <script type="text/javascript" src="/assets/libs/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/assets/js/script.js"></script>
</head>
<body>

<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="/">BeeJee Test</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li><a href="/" class="active">Home</a></li>
                <li><a href="/reviews">Reviews</a></li>
                <?php if (isset($user_auth) && $user_auth) { ?>
                    <li><a href="/login/logout">Logout</a></li>
                <?php } else { ?>
                    <li><a href="/login">Login</a></li>
                <?php } ?>
            </ul>
        </div><!--/.nav-collapse -->
    </div>
</nav>
<div class="jumbotron">
    <div class="container">
        <h2><?php echo (isset($data['title'])) ? $data['title'] : 'BeeJee Test'; ?></h2>
    </div>
</div>
<div class="content-wrapper">