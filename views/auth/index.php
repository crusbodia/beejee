<?php

require_once BATH_PATH . '/views/inc/header.php'; ?>

<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <form action='/login/auth' method='post'>
                <div class="form-group">
                    <label for="login">Login: </label>
                    <input type="text" class="form-control" name="login" id="login" placeholder="Login" value="">
                </div>
                <div class="form-group">
                    <label for="password">Password: </label>
                    <input type="password" class="form-control" name="password" id="password" placeholder="Password" value="">
                </div>
                <button class="btn-block btn btn-success" type="submit">Submit</button>
            </form>
        </div>
    </div>
</div>

<?php require_once BATH_PATH . '/views/inc/footer.php';